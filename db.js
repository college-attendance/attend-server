const Redis = require('ioredis')
const redis = new Redis()

exports.studentFromId = async (id) => {
  const result = await redis.get(`student:${id}`)
  return JSON.parse(result)
}

exports.lastScan = async (id) => {
  const results = await redis.zrange(`history:${id}`, -1, -1)

  if (results.length === 0) {
    return null
  }

  const entry = JSON.parse(results[0])
  return entry
}

exports.makeScan = (entering, id) => {
  const now = Date.now()
  return { entering, date: now, id }
}

exports.writeScan = async (scan) => {
  await redis.zadd(
    `history:${scan.id}`,
    scan.date,
    JSON.stringify(scan, null, 2),
  )
}

exports.lastSync = async (clientId) => {
  return await redis.get(`last-sync:${clientId}`)
}

exports.writeScans = async (scans) => {
  const pipeline = redis.pipeline()
  scans.forEach((scan) => {
    const scanData = JSON.parse(scan)
    console.log('scanData', scanData)
    pipeline.zadd(`history:${scanData.id}`, scanData.date, scan, (err) => {
      if (err) {
        console.log(err)
        // throw new Error('Error sending command pipeline')
      }
    })
  })
  await pipeline.exec()
}

exports.writeLastSync = async (clientId, date) => {
  await redis.set(`last-sync:${clientId}`, date)
}
