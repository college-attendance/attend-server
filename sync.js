const Nano = require('nano')
const { getClasses, getAllPeople, getUpdatedPeople } = require('./populi')
const { loadConfig } = require('./config')
const _ = require('lodash')
const debug = require('debug')
const pMap = require('p-map')
const objectHash = require('object-hash')
const cuid = require('cuid')

const syncClasses = async () => {
  const nano = await getNano()
  const classesDB = await nano.use('classes')
  const log = debug('attend-server:syncClasses')

  // get classes from Populi
  log('getting classes from Populi')
  const populiClasses = await getClasses()

  // send to couchdb
  await syncToMatch(classesDB, populiClasses)
  log('finished complete classes sync.')
}
exports.syncClasses = syncClasses

const syncStudents = async () => {
  const nano = await getNano()
  const students = await nano.use('students')
  const meta = await nano.use('meta')
  const log = debug('attend-server:syncStudents')
  const syncDate = Date.now()

  const lastSyncDoc = await grace(meta.get('last_sync'))

  if (lastSyncDoc) {
    const shouldRefreshDoc = await grace(meta.get('should_refresh_students'))
    if (shouldRefreshDoc && shouldRefreshDoc.value === true) {
      // begin refresh sync
      log('refreshing db entries for existing students')

      // get all registered students from CouchDB
      const body = await students.list({ include_docs: true })
      const studentsInDB = body.rows.map((row) => row.id)

      if (studentsInDB.length === 0) {
        log('no registered students found')
        return
      }

      log(`Updating entries for ${studentsInDB.length} students.`)

      // we started with the full list of entries
      // then we re-fetched them from Populi, including deletes
      // now we replace the db's contents with the updated entries list
      // finally we will perform a delta sync to get any updates since we started
      const entriesFilter = new Set(studentsInDB)
      const populiEntries = await getAllPeople(entriesFilter)
      await syncToMatch(students, populiEntries)

      // clear the should_refresh flag
      await meta.insert(
        Object.assign({}, shouldRefreshDoc, { value: false }),
        'should_refresh_students',
      )

      log('finished complete students sync.')
    }

    // begin delta sync

    // check since 10 minutes ago
    const adjustedLastSyncDate = lastSyncDoc.date - 1000 * 60
    log('syncing changes since', adjustedLastSyncDate)

    // get updated people
    const updatedPeople = await getUpdatedPeople(adjustedLastSyncDate)

    if (updatedPeople.length === 0) {
      log('no changes found to registered students')
      return
    }

    log(
      'saving updates for',
      updatedPeople
        .map((person) => [person.firstName, person.lastName].join(' '))
        .join(', '),
    )

    // // compute update operations
    // const couchOps = await pMap(
    //   updatedPeople,
    //   async (person) => {
    //     // get the person from couchDB
    //     const existingPerson =
    //       (await grace(students.get(person.personId))) || {}
    //
    //     // create a new user object, carrying over _rev from the existing person
    //     // and using Populi's person id as the couchDB id
    //     // CouchDB requires the _rev field for updates and deletes
    //     // this also handles deletion becuase _deleted will be set
    //     return Object.assign(
    //       {},
    //       { _rev: existingPerson._rev, _id: person.personId },
    //       person,
    //     )
    //   },
    //   { concurrency: 10 },
    // )
    //
    // // apply the changes, if there are any
    // await applyCouchOps(students, couchOps)

    await syncToMatch(students, updatedPeople)

    log('finished delta sync')
  } else {
    // run a full sync
    log('running a complete sync')

    // get classes from Populi
    log('getting entries from Populi')
    const populiEntries = await getAllPeople()

    log('saving entries to couchdb')
    await syncToMatch(students, populiEntries, { deleteMissing: true })
    log('finished complete students sync.')
  }

  // there are some edge cases here, if two sync servers start at once
  // IMO not worth coding for, because they will self resolve.
  // Note that hard sync errors must throw to avoid updating the last_sync date
  log('saving new sync date')
  await meta.insert(
    Object.assign({}, lastSyncDoc, { date: syncDate }),
    'last_sync',
  )
}
exports.syncStudents = syncStudents

// make couchDB match the target array of documents
// adding, deleting, or updating as needed
// assumes that the target array does not have ids yet.
const syncToMatch = async (
  db,
  masterEntries,
  { deleteMissing = false } = {},
) => {
  const log = debug('attend-server:syncToMatch')

  log(`got ${masterEntries.length} master entries`)

  // couchdb ops list
  // defined up here for deduping
  // note that this algorithm isn't expected to create duplicates,
  // but at one point the code was creating thousands of duplicates and I
  // needed a way to get rid of them
  const couchOps = []

  log('loading couchdb entries')
  // get entries from CouchDB, and build a map
  const couchEntries = new Map()
  const body = await db.list({ include_docs: true })
  body.rows.forEach(({ doc }) => {
    const hash = contentHash(doc)

    // Entry we're currently considering is a duplicate
    // of another entry we've previously considered
    if (couchEntries.has(hash)) {
      // delete it from the DB
      couchOps.push(Object.assign({}, doc, { _deleted: true }))
      // early return
      return
    }

    couchEntries.set(hash, doc)
  })

  // build couchdb ops list
  // (see Readme.md for a description of this algorithm)
  log('computing diff')

  // add operations
  masterEntries.forEach((entry) => {
    // compute the content hash for this entry
    const hash = contentHash(entry)

    // Map.prototype.delete returns the old entry or false if it didn't exist
    const alreadyInDb = couchEntries.delete(hash)

    // add the new entry, if it wasn't in the db already
    if (!alreadyInDb) {
      // generate a new unique, semi-sequential,
      // immutable, collision-resistant id
      couchOps.push(Object.assign({}, entry, { _id: cuid() }))
    }
  })

  // delete operations
  if (deleteMissing) {
    couchEntries.forEach((entry) => {
      // we don't need to assign _id,
      // because the object from CouchDB already has it
      couchOps.push(Object.assign({}, entry, { _deleted: true }))
    })
  }

  await applyCouchOps(db, couchOps)
}

// const createContentHashMap = (documents) => {
//   const kvPairs = documents.map((doc) => [contentHash(doc), doc])
//   const contentMap = new Map(kvPairs)
//   return contentMap
// }

// get the hash of a couchdb document's contents
// ignoring meta fields like revision and id
const contentHash = (doc) => {
  // get rid of meta fields
  const onlyContent = stripMeta(doc)

  // hash it
  const contentHash = hashObject(onlyContent)

  return contentHash
}

// Create a new object ommitting couchdb meta fields
// (they start with an underscore)
const stripMeta = (doc) => {
  return _.omitBy(doc, (_value, key) => key.startsWith('_'))
}

const applyCouchOps = async (db, couchOps) => {
  const log = debug('attend-server:applyCouchOps')
  if (couchOps.length) {
    const deleted = couchOps.filter((o) => o._deleted).length
    const added = couchOps.length - deleted
    log(
      `Sending ${
        couchOps.length
      } operations to CouchDB.  ${deleted} deleted, ${added} added.`,
    )
    return await db.bulk({ docs: couchOps })
  } else {
    log('DB is already in sync.  Nothing to do.')
  }
}

const hashObject = (obj) => {
  const sanitisedObject = _.omitBy(obj, (v) => v === undefined)
  return objectHash(sanitisedObject, { encoding: 'base64' })
}

const grace = async (promise, allowed_errors = ['not_found']) => {
  try {
    return await promise
  } catch (e) {
    if (allowed_errors.includes(e.error)) {
      return null
    } else {
      throw e
    }
  }
}

const getNano = _.once(async () => {
  const config = await loadConfig()
  return Nano(config.couchdbURL)
})

// function couchUrl() {
//   const name = process.env.COUCHDB_USER
//   const pass = process.env.COUCHDB_PASS
//   return `http://${name}:${pass}@localhost:5984`
// }
