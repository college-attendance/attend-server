const hardRejection = require('hard-rejection')
const { syncStudents, syncClasses } = require('./sync')
const { asyncInterval } = require('./util')

hardRejection()

const main = async () => {
  // sync classes from populi to couchdb
  // every 10 seconds
  asyncInterval(30000, async () => {
    await syncClasses()
  })

  // sync students from populi to couchdb
  // every 10 seconds
  asyncInterval(2500, async () => {
    await syncStudents()
  })

  // record attendance in populi from couchdb records
}

main()
