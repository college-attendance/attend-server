const delay = require('delay')

// run a task every n milliseconds
// if the task takes longer than n to execute, just wait for it to finish
const asyncInterval = (interval, task) => {
  // Start when the thread becomes ideal
  setTimeout(async () => {
    // infinite loop
    for (;;) {
      // when the next task should start
      let nextStart = Date.now() + interval

      // run the taxk
      await task()

      // wait until the next start time
      const timeTillNextRun = nextStart - Date.now()

      // only wait if target time is in the future
      if (timeTillNextRun > 0) {
        await delay(timeTillNextRun)
      }
    }
  }, 0)
}

exports.asyncInterval = asyncInterval
