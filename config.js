const cosmiconfig = require('cosmiconfig')
const name = require('./package.json').name

const explorer = cosmiconfig(name)

const loadConfig = async () => {
  const localConfig = await explorer.search()
  if (localConfig) {
    return localConfig.config
  } else {
    return await explorer.load(`/etc/${name}/${name}.config.js`)
  }
}
exports.loadConfig = loadConfig
