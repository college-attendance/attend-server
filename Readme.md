# Attendance Server

- Runs CouchDB as the central source of truth.
- Serves the web ui for the scanner.
- Node app keeps CouchDB in sync with Populi

## CouchDB

Several Databases:

- Scans database, which is read/write for all clients.
- Students database, which maps name -> card ID. Read-only for clients.
- Classes database, which contains a list of classes. Read-only for clients.

Security considerations:

- We are not defending against spurious scans caused by a compromise of the device itself. Anyone could pull the SD card and inject malicious code, and there is nothing practical we can do about that short of using something other than a Raspberry Pi.
- The devices will go on a dedicated VLAN which has exclusive access to the attendance server, no connection with the rest of campus, and no access to the internet unless we enable it temporarily for updates, etc. This defends against malicious CouchDB clients connecting from other locations, and also demotivates using the Raspberry Pis as access points, proxies, etc.
- We will keep regular backups of the attendance database in case a malicious client tries to delete everything.

## Static server

Caddy Server is recommended, because it automatically configures TLS.

## Node app

### Classes Synchronization

No delta sync api is available for meetings or classes, so we download a full copy every time we sync. Instead of creating one document per meeting and a separate classes, we create one document per class and include meetings as a sub-array. This makes synchronization much easier.

To synchronize the class list:

- Get everything from the DB and store it in a HashMap
- Save everything from Populi in a second HashMap
- Build up a list of bulk operations to make the first HashMap look like the second
- Send to CouchDB, ignoring `_rev` errors.

To compute the difference:

- For every entry in Populi
  - Check if it already exists in the DB.
  - If it doesn't, push an "add" operation
  - If it does, remove it from the CouchDB hashmap
- For every remaining couchDB entry
  - push a remove operation

To compute hashes to be used as Map keys:

- Create a new document with known keys. Unknown keys will be dropped.
- Extract all fields into a string in hardcoded order. This is possible because the list of fields is known.
- hash the string using hasha

### Students Synchronization

Delta sync is fortunately available for student details. We use this to avoid hammering Populi with 300+ requests every few minutes.

To perform a delta sync:

- Ask Populi for a list of students since last sync
- For each changed student:
  - If the update is a deletion, then fetch the most recent revision from couchDB and convert it to a deletion.
  - For all other updates, fetch the student details from Populi and upsert that into CouchDB.

Fresh syncs are performed in the same way as class sync.

Students actually have a third sync mode called refresh. The basic idea is, let's say you need to scrape a new field for all existing students, but don't want to hammer Populi with 800+ requests and fight with rate limiting. Refresh sync is your friend. To enable it, set should_refresh_students to true in the meta table of couchdb.

Refresh sync must not advance the last sync timestamp, since it only handles students in the db and does not account for added students since the last sync.

## Saving attendance to Populi

Each scan has a `savedToPopuli`. The server regularly queries the DB for scans without this field and uploads them to Populi. Once they are verified as recorded, the server sets `savedToPopuli = true`.

The classId / studentId to which a scan applies is computed on the client to ensure parity between what the client shows on it's screen and what gets logged to Populi. Populi doesn't always include a meetingID, so we generate our own during the syncronization process.

They will include userID, badge id, time, date, and a couchdb `_id` pointing to the meeting which they believe the scan applies to.

The full process looks something like this for each scan:

- Look up full list of scans.
- Check populi to see if attendance tracking has been started
  Before registering a scan, it's necessary to check attendance. if attendance has previously been tracked with this meeting
