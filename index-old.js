const Koa = require('koa')
const IO = require('koa-socket')

const { writeScan, lastSync, writeScans, writeLastSync } = require('./db')
const { startSync } = require('./sync')

const app = new Koa()
const io = new IO()

app.use(async (ctx) => {
  ctx.body = 'Hello World'
})

io.attach(app)

io.on('connection', ({ socket }) => {
  console.log('user connected')

  socket.on('scan', (data) => {
    console.log(data)
    writeScan(data)
  })

  socket.on('sync-request', (clientId, cb) => {
    console.log('received sync request from', clientId)
    lastSync(clientId).then((last) => {
      console.log('last sync was', Date.now() - last, 'ms ago')
      cb(Number(last))
    })
  })

  socket.on('sync', async (clientId, scans) => {
    console.log('syncing with', clientId)
    await writeScans(scans)
    await writeLastSync(clientId, Date.now())
    console.log(`wrote ${scans.length} entries`)
  })

  socket.on('disconnect', () => {
    console.log('user disconnected')
  })
})

const listenPort = process.env.PORT || 3400
app.listen(listenPort, (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log(`Koa listening on *:${listenPort}`)
  }
})
