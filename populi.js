const { loadConfig } = require('./config')
const FormData = require('form-data')
const fetch = require('node-fetch')
const xml2js = require('xml2js')
const dayjs = require('dayjs')
const debug = require('debug')
const pMap = require('p-map')
const ProgressBar = require('progress')
const promiseRetry = require('promise-retry')

// Downloads the full student list
const getAllPeople = async (filter = null) => {
  const log = debug('attend-server:getAllPeople')
  const config = await loadConfig()

  // get a listing of all users in the system
  log('Downloading user list.')
  const getUsersResult = await callPopuli(config, { task: 'getUsers' })
  let records = getUsersResult.response.person
  log(`Got ${records.length} users.`)

  // clean up the data
  records = records.map((user) => {
    return {
      firstName: user.first[0],
      lastName: user.last[0],
      personId: user.person_id[0],
      username: user.username[0],
    }
  })

  // filter data
  if (filter) {
    records = records.filter((r) => filter.has(r.personId))
  }

  // look up custom fields and filter out users without badge ids
  return await enrichPeople(records)
}
exports.getAllPeople = getAllPeople

// Downloads the updated student list
const getUpdatedPeople = async (lastSync) => {
  const log = debug('attend-server:getUpdatedPeople')
  const config = await loadConfig()

  // get a listing of all updated users
  const lastSyncStamp = dayjs(lastSync).format('YYYY-MM-DD HH:mm:ss')
  log('Fetching people updated since', lastSyncStamp)
  const getUsersResult = await callPopuli(config, {
    task: 'getUpdatedPeople',
    start_time: lastSyncStamp,
  })

  // return early if no updates
  if (getUsersResult.response.length === 0) {
    return []
  }

  // split into updated and deleted users
  const people = getUsersResult.response.person
  const updatedPeople = []
  const deletedPeople = []
  people.forEach((person) => {
    if (person.status[0] === 'DELETED') {
      deletedPeople.push({ personId: person.id[0], _deleted: true })
    } else {
      updatedPeople.push(person)
    }
  })
  log(
    `Got ${updatedPeople.length} updated users and ${
      deletedPeople.length
    } deleted users.`,
  )

  // de-xmlize the returned users
  const cleanRecords = updatedPeople.map((user) => {
    return {
      firstName: user.first_name[0],
      lastName: user.last_name[0],
      personId: user.id[0],
      username: user.user_name[0].replace('@', ''),
    }
  })

  // enrich the updated users
  const enrichedPeople = await enrichPeople(cleanRecords)

  // return deletes and updates separately
  return enrichedPeople.concat(deletedPeople)
}
exports.getUpdatedPeople = getUpdatedPeople

// take a list of people
// look up custom fields
// filter out people without custom fields
// also add information about which classes this person attends.
const enrichPeople = async (people) => {
  const log = debug('attend-server:enrichPeople')
  const config = await loadConfig()

  // look up badge id (if it exists) for each user
  log('Getting custom fields.')
  const bar = new ProgressBar(':current/:total :etas [:bar]', {
    total: people.length,
  })
  people = await pMap(
    people,
    async (user) => {
      const newUser = Object.assign({}, user, { badgeId: '' })

      // call populi
      const getCustomFieldsResult = await callPopuli(config, {
        task: 'getCustomFields',
        person_id: newUser.personId,
      })

      // increment the progress bar
      bar.tick()

      // show appropriate errors on rate limiting
      if (typeof getCustomFieldsResult.response == 'undefined') {
        log('rate limit err:', getCustomFieldsResult)
      }

      // handle users without custom fields
      if (typeof getCustomFieldsResult.response.custom_field == 'undefined') {
        return newUser
      }

      // now attempt to extract the badge id
      const customFields = getCustomFieldsResult.response.custom_field
      const badgeId = customFields.find((f) => f.name[0] === 'Badge ID')
      if (badgeId) {
        newUser.badgeId = badgeId.value[0]
      }

      return newUser
    },
    { concurrency: 100 },
  )

  // filter out users without custom fields
  log('filtering users without badges')
  people = people.filter((u) => !!u.badgeId)
  log(`${people.length} registered users.`)

  // look up enrollment for remaining students
  log('adding enrollment data')
  const enrollmentMap = await getEnrollmentMap()
  people = people.map((user) => {
    // console.log(user)
    const enrolledClasses = enrollmentMap.get(user.personId) || []
    const newUser = Object.assign({}, user, { enrolledClasses })
    return newUser
  })

  return people
}

// returns a Map with student ids as keys and classes as values
const getEnrollmentMap = async () => {
  const config = await loadConfig()

  const map = new Map()

  // for each configured class
  await Promise.all(
    config.classes.map(async (instanceID) => {
      // get a list of enrolled students
      const students = await getCourseInstanceStudents(instanceID)

      // for each enrolled student
      students.forEach((student) => {
        // get the list of other classes this student is enrolled in
        // created by previous iterations of the outer loop
        // default to empty array
        const currentValue = map.get(student.personId) || []

        // create a new array including this class
        const newValue = [
          ...currentValue,
          {
            status: student.status,
            startDate: student.startDate,
            courseInstanceId: instanceID,
          },
        ]

        // add it to the main map
        map.set(student.personId, newValue)
      })
    }),
  )

  // we're done!
  return map
}

// given an instance id, return a list of registered students
const getCourseInstanceStudents = async (instanceId) => {
  const config = await loadConfig()
  // const log = debug('attend-server:getClassInstanceStudents')

  const response = (await callPopuli(config, {
    task: 'getCourseInstanceStudents',
    instanceID: instanceId,
  })).response

  const students = response.courseinstance_student.map((s) => {
    return {
      status: s.status[0],
      personId: s.personid[0],
      startDate: s.start_date[0],
    }
  })

  return students
}

const getClasses = async () => {
  const config = await loadConfig()

  let sessions = []

  await Promise.all(
    config.classes.map(async (instanceId) => {
      const classInfo = await getClassInfo(config, instanceId)
      const classes = await getClassMeetings(config, classInfo, instanceId)
      sessions = sessions.concat(classes)
    }),
  )
  return sessions
}
exports.getClasses = getClasses

const getClassInfo = async (config, instanceId) => {
  const response = (await callPopuli(config, {
    task: 'getCourseInstance',
    instanceID: instanceId,
  })).response

  return {
    name: response.name[0],
    abbreviation: response.abbrv[0],
  }
}
exports.getClassInfo = getClassInfo

const getClassMeetings = async (config, classInfo, instanceId) => {
  const response = await callPopuli(config, {
    task: 'getCourseInstanceMeetings',
    instanceID: instanceId,
  })

  return response.response.meeting.map((meeting) => {
    let meetingId
    if (meeting.meetingid) {
      meetingId = meeting.meetingid[0]
    }

    return {
      classInstanceId: instanceId,
      className: classInfo.name,
      classAbbrev: classInfo.abbreviation,
      counts_toward_attendance_hours: meeting.counts_toward_attendance_hours[0],
      counts_toward_clinical_hours: meeting.counts_toward_clinical_hours[0],
      end: dayjs(meeting.end[0]).valueOf(),
      start: dayjs(meeting.start[0]).valueOf(),
      roomId: meeting.roomid[0],
      meetingId,
    }
  })
}
exports.getClassMeetings = getClassMeetings

const callPopuli = async (config, params) => {
  const formData = new FormData()

  const { accessKey, endpoint } = config

  // append API key
  formData.append('access_key', accessKey)

  // append custom params
  Object.keys(params).forEach((key) => {
    formData.append(key, params[key])
  })

  const doCall = async (retry) => {
    const response = await fetch(endpoint, {
      method: 'POST',
      body: formData,
    }).catch(retry)

    if (response.error) {
      throw new Error(response.error)
    }

    const body = await response.text()
    const result = await parseXmlString(body)

    if (result.error && result.error.code[0] === '429') {
      return retry()
    }

    if (result.error) {
      throw new Error(result.error.message)
    }

    return result
  }

  const retryResult = await promiseRetry(doCall, {
    randomize: true,
    minTimeout: 30000,
    maxTimeout: 60000,
  })

  return retryResult
}
exports.callPopuli = callPopuli

const parseXmlString = (xmlString) => {
  return new Promise((resolve, reject) => {
    xml2js.parseString(xmlString, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}
